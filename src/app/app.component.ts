import { LoginPage } from '../pages/login/login';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { ResultPage } from '../pages/result/result';
import { HistoryPage } from '../pages/history/history';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar,private splashScreen: SplashScreen,
    private translate: TranslateService) {
    platform.ready().then(() => {
      this.initTranslate()
      statusBar.styleDefault();
      this.hideSplashScreen();
    });
  }

  hideSplashScreen()
  {
    if (SplashScreen)
    {setTimeout(()=> {
      this.splashScreen.hide();
    }, 100);}
  }

  initTranslate() {
    this.translate.setDefaultLang('es');
    if (this.translate.getBrowserLang() !== undefined) {
      //this.translate.use(this.translate.getBrowserLang());
      this.translate.use('es'); // Descomentar cuando exista el archivo 'en.json'
    } else {
      this.translate.use('es'); // Set your language here
    }
  }
}

