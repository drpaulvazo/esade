import {QuizPage} from '../pages/quiz/quiz';
import {QuestionPage} from '../pages/question/question';
import {LoginPage} from '../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginProvider } from '../providers/login.service';
import { MenuPage } from '../pages/menu/menu';
import { ListaPage } from '../pages/lista/lista';
import { RegistroPage } from '../pages/registro/registro';
import { ChildrenProvider } from '../providers/children/children';
import { QuestionsProvider } from '../providers/questions/questions.service';
import { TranslateModule } from '@ngx-translate/core';
import { TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HistoryPage } from '../pages/history/history';
import { ResultPage } from '../pages/result/result';
import { HistoryProvider } from '../providers/history/history';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    MenuPage,
    RegistroPage,
    ListaPage,
    QuestionPage,
    QuizPage,
    HistoryPage,
    ResultPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,{
      monthShortNames: ['ene', 'feb', 'mar', 'abr','may','jun','jul','ago','sep','oct','nov','dic']
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    MenuPage,
    RegistroPage,
    ListaPage,
    QuestionPage,
    QuizPage,
    HistoryPage,
    ResultPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoginProvider,
    ChildrenProvider,
    QuestionsProvider,
    HistoryProvider,
    HistoryProvider
  ]
})
export class AppModule {}
