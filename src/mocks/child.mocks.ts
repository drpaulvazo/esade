import { Child } from '../models/child.interface';

const childList: Child[] = [
    {
        idChild:1,
        names: 'Paul Wilker',
        lastName: 'Landaeta',
        secondName: 'Flores',
        sexo: 'masculino',
        peso: '60',
        talla: '170',
        birthDate: '10/10/2017'
    },
    {
        idChild:2,        
        names: 'Elfy Jhoselyne',
        lastName: 'Lazo',
        secondName: 'Monroy',
        sexo: 'Femenino',
        peso: '50',
        talla: '160',
        birthDate: '08/10/2017'
    },
    {
        idChild:3,        
        names: 'Ivan Rodrigo',
        lastName: 'Lazo',
        secondName: 'Monroy',
        sexo: 'masculino',
        peso: '50',
        talla: '160',
        birthDate: '12/10/2017'
    },
    {
        idChild:4,        
        names: 'Fernanda ',
        lastName: 'Alvarez',
        secondName: 'Flores',
        sexo: 'Femenino',
        peso: '50',
        talla: '160',
        birthDate: '11/25/2016'
    },
    {
        idChild:5,
        names: 'Luis Angel',
        lastName: 'Lazo',
        secondName: 'Monroy',
        sexo: 'masculino',
        peso: '50',
        talla: '160',
        birthDate: '02/10/2017'
    },
    {
        idChild:6,
        names: 'Elmer Zapata',
        lastName: 'Perez',
        secondName: 'Monroy',
        sexo: 'masculino',
        peso: '50',
        talla: '160',
        birthDate: '04/10/2015'
    },
    {
        idChild:7,        
        names: 'Tania',
        lastName: 'Lazo',
        secondName: 'Monroy',
        sexo: 'masculino',
        peso: '50',
        talla: '160',
        birthDate: '02/09/2016'
    },
    {
        idChild:8,        
        names: 'Ivan Rodrigo',
        lastName: 'Lazo',
        secondName: 'Monroy',
        sexo: 'masculino',
        peso: '50',
        talla: '160',
        birthDate: '02/24/2015'
    },
    {
        idChild:9,                
        names: 'Ivan Rodrigo',
        lastName: 'Lazo',
        secondName: 'Monroy',
        sexo: 'masculino',
        peso: '50',
        talla: '160',
        birthDate: '03/10/2017'
    },
    {
        idChild:10,                
        names: 'Ivan Rodrigo',
        lastName: 'Lazo',
        secondName: 'Monroy',
        sexo: 'masculino',
        peso: '50',
        talla: '160',
        birthDate: '06/25/2017'
    },
   
]

export const CHILD_LIST = childList;