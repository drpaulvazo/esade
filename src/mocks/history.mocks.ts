import { History } from '../models/history.interface';

const historyList: History[] = [
    {
        idChild: 1,
        date: '02/02/2017',
        edadMeses: 3,
        total: 18,
        mf:12,
        mg:10,
        al:2,
        ps:3
    },
    {
        idChild: 2,
        date: '10/05/2017',
        edadMeses: 8,
        total: 20,
        mf:11,
        mg:5,
        al:9,
        ps:10
    },
]

export const HISTORY_LIST = historyList;