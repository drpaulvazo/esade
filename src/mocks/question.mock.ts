import { Question } from '../models/question.interface';

const questionList: Question[] = [
    {
        month: 0,
        title: "PATALEA FUERTE",
        message: "Llame su atención con un objeto. Trate de que la niña o el niño lo patee.",
        type: "MG",
        image: "0a"
    },
    {
        month: 0,
        title: "SIGUE LOS MOVIMIENTOS DE UN OBJETO (2 VECES)",
        message: "Muéstrele un objeto moviéndolo de derecha a izquierda y de arriba hacia abajo.",
        type: "MF",
        image: "0b"
    },
    {
        month: 0,
        title: "SE ASUSTA CON UN RUIDO",
        message: "Hágale echar, a cierta distancia hagaun ruido.",
        type: "AL",
        image: "0c"
    },
    {
        month: 0,
        title: "SIGUE LOS MOVIMIENTOS DE LA CARA",
        message: "Ubíquese a medio metro, mueva su cara de un lado a otro. La niña o el niño debe seguirle con la mirada.",
        type: "PS",
        image: "0d"
    },
    {
        month: 1,
        title: "LEVANTA Y MANTIENE LA CABEZA (HASTA CONTAR 3)",
        message: "Póngale de barriga y llame su atención con un objeto.",
        type: "MG",
        image: "1a"
    },
    {
        month: 1,
        title: "ABRE Y MIRA SUS MANOS",
        message: "La niña o el niño lleva una o las dos manos delante de sus ojos y las mira.",
        type: "MF",
        image: "1b"
    },
    {
        month: 1,
        title: "BUSCA EL SONIDO CON LA MIRADA",
        message: "Haga sonar un objeto sin que lo vea. La niña o el niño debe buscar el sonido con la mirada.",
        type: "AL",
        image: "1c"
    },
    {
        month: 1,
        title: "RECONOCE A LA MADRE",
        message: "Pidale a la madre que se acerque a la niña o el niño. La niña o el niño debe mirarla fijamente y sonreír.",
        type: "PS",
        image: "1d"
    },
    {
        month: 2,
        title: "LEVANTA Y MANTIENE LA CABEZA Y PECHO (HASTA CONTAR 3)",
        message: "Póngale de barriga y llame su atención con un objeto.",
        type: "MG",
        image: "2a"
    },
    {
        month: 2,
        title: "SOSTIENE UN OBJETO CON LA MANO (HASTA CONTAR 10)",
        message: "Dele un objeto liviano para que agarre con la mano.",
        type: "MF",
        image: "2b"
    },
    {
        month: 2,
        title: "PRONUNCIA 2 SONIDOS DIFERENTES",
        message: "Escuche los sonidos que hace la niña o el niño . Debe hacer por lo menos 2 sonidos diferentes: ggg, uuuu, etc.",
        type: "AL",
        image: "2c"
    },
    {
        month: 2,
        title: "SONRÍE CUANDO SE LE ACARICIA",
        message: "Acaricie la cara de la niña o el niño, él o ella debe mirar y sonreír.",
        type: "PS",
        image: "2d"
    },
    {
        month: 3,
        title: "MANTIENE FIRME LA CABEZA AL LEVANTARLO (3 VECES )",
        message: "Hágale recostar de espalda, y sosteniéndolo de sus axilas levántelo despacio.",
        type: "MG",
        image: "3a"
    },
    {
        month: 3,
        title: "LLEVA OBJETOS A LA BOCA (3 VECES)",
        message: "Dele un objeto liviano y seguro, y observe si lo lleva a la boca.",
        type: "MF",
        image: "3b"
    },
    {
        month: 3,
        title: "BALBUCEA CON LAS PERSONAS",
        message: "Muéstrele un objeto llamativo. La niña o el niño debe reaccionar haciendo sonidos como queriendo hablar.",
        type: "AL",
        image: "3c"
    },
    {
        month: 3,
        title: "GIRA CUANDO ESCUCHA HABLAR",
        message: "Hable con la madre o el padre cerca de la niña o el niño, observe si gira cuando escucha hablar.",
        type: "PS",
        image: "3d"
    },
    {
        month: 4,
        title: "MANTIENE FIRME LA CABEZA MIENTRAS ESTÁ SENTADA",
        message: "Sosteniendo sus manos o espalda, hágale sentar.",
        type: "MG",
        image: "4a"
    },
    {
        month: 4,
        title: "AGARRA OBJETOS",
        message: "Hágale sentar, ponga varios objetos pequeños delante de la niña o el niño.",
        type: "MF",
        image: "4b"
    },
    {
        month: 4,
        title: "PRONUNCIA 4 SONIDOS DIFERENTES",
        message: "Escuche los sonidos que hace la niña o el niño. Debe hacer: ggg, uu, rrr, sss, etc.",
        type: "AL",
        image: "4c"
    },
    {
        month: 4,
        title: "AGARRA LA MANO DEL EVALUADOR",
        message: "Acerque sus manos a las de la niña o el niño, y ella o él debe mirar y agarrarlas.",
        type: "PS",
        image: "4d"
    },
    {
        month: 5,
        title: "GIRA DE UN LADO AL OTRO",
        message: "Póngale de espaldas y desde un lado llame su atención con un objeto para que gire.",
        type: "MG",
        image: "5a"
    },
    {
        month: 5,
        title: "AGARRA UN OBJETO EN CADA MANO (HASTA CONTAR 3)",
        message: "Hágale sentar con apoyos, dele dos objetos, uno en cada mano. Debe agarrar sin soltarlos.",
        type: "MF",
        image: "5b"
    },
    {
        month: 5,
        title: "RÍE A CARCAJADAS",
        message: "Hágale algunos gestos o cosquillas suaves, o escuche mientras juega. Debe reír fuerte.",
        type: "AL",
        image: "5c"
    },
    {
        month: 5,
        title: "ACEPTA Y AGARRA EL JUGUETE",
        message: "Ofrézcale un juguete. La niña o el niño debe mirar y luego animarse a agarrar el juguete.",
        type: "PS",
        image: "5d"
    },
    {
        month: 6,
        title: "TRATA DE SENTARSE SOLA O SOLO (2 VECES)",
        message: "Póngale de espaldas y llame su atención con un objeto.",
        type: "MG",
        image: "6a"
    },
    {
        month: 6,
        title: "PASA UN OBJETO DE UNA MANO A OTRA",
        message: "Hágale sentar sin apoyos, dele un objeto. La niña o el niño debe pasarlo de una mano a la otra.",
        type: "MF",
        image: "6b"
    },
    {
        month: 6,
        title: "GIRA LA CABEZA CUANDO SE LE LLAMA",
        message: "Llámele por su nombre sin que le vea. La niña o el niño debe darse la vuelta mirando a la dirección correcta.",
        type: "AL",
        image: "6c"
    },
    {
        month: 6,
        title: "PONE ATENCIÓN A LA CHARLA",
        message: "Hable con la persona que cuida a la niña o niño muy cerca de ella o de él. Debe mirar atentamente.",
        type: "PS",
        image: "6d"
    },
    {
        month: 7,
        title: "SE MANTIENE SENTADA O SENTADO CON APOYO (HASTA CONTAR 3)",
        message: "Hágale sentar con apoyo, y luego retire despacio el apoyo.",
        type: "MG",
        image: "7a"
    },
    {
        month: 7,
        title: "AGARRA VARIOS OBJETOS AL MISMO TIEMPO",
        message: "Hágale sentar con apoyo, dele varios objetos que pueda agarrarlos.",
        type: "MF",
        image: "7b"
    },
    {
        month: 7,
        title: "PRONUNCIA 3 SÍLABAS",
        message: "Escuche los sonidos con vocales. Debe decir: ma, ta, pa, etc.",
        type: "AL",
        image: "7c"
    },
    {
        month: 7,
        title: "AYUDA A AGARRAR LA TAZA PARA TOMAR",
        message: "Pregunte a la madre o al padre si la niña o el niño toma en vaso o taza y si ayuda a agarrar.",
        type: "PS",
        image: "7d"
    },
    {
        month: 8,
        title: "SE ARRASTRA PARA ALCANZAR UN OBJETO",
        message: "Póngale de barriga y muéstrele un objeto. Debe hacer fuerza en brazos y piernas para poder alcanzarlo.",
        type: "MG",
        image: "8a"
    },
    {
        month: 8,
        title: "AGARRA UN OBJETO PEQUEÑO CON LOS DEDOS",
        message: "Dele un objeto pequeño. La niña o el niño debe agarrar el objeto sólo con los dedos sin apoyarlo en la palma.",
        type: "MF",
        image: "8b"
    },
    {
        month: 8,
        title: "HACE SONAR LA SONAJERA (3 VECES)",
        message: "Muéstrele cómo hacer sonar la sonajera. La niña o el niño debe moverla para hacerla sonar.",
        type: "AL",
        image: "8c"
    },
    {
        month: 8,
        title: "REACCIONA AL VERSE EN EL ESPEJO",
        message: "Coloque un espejo delante de la niña o el niño. Debe mirar y tratar de tocar su imagen y sonreír.",
        type: "PS",
        image: "8d"
    },
    {
        month: 9,
        title: "SE SIENTA SIN AYUDA",
        message: "Hágale recostar de espaldas, llame su atención con un objeto. Debe sentarse sola o solo.",
        type: "MG",
        image: "9a"
    },
    {
        month: 9,
        title: "AGARRA UN OBJETO CON LOS DEDOS PULGAR E ÍNDICE",
        message: "Dele un objeto. La niña o el niño debe agarrar solamente con dos dedos.",
        type: "MF",
        image: "9b"
    },
    {
        month: 9,
        title: "DICE UNA PALABRA CLARAMENTE",
        message: "Escuche a la niña o al niño, o pregunte a la familia. Si la niña o el niño dice claramente: mamá, papá, etc.",
        type: "AL",
        image: "9c"
    },
    {
        month: 9,
        title: "IMITA APLAUSOS (2 VECES)",
        message: "Aplauda frente a la niña o el niño. Ella o él debe hacer lo mismo.",
        type: "PS",
        image: "9d"
    },
    {
        month: 10,
        title: "GATEA (2 METROS DE DISTANCIA)",
        message: "Póngale en posición de gateo, llame su atención con un objeto.",
        type: "MG",
        image: "10a"
    },
    {
        month: 10,
        title: "METE Y SACA OBJETOS DE UN RECIPIENTE",
        message: "Dele un recipiente lleno de objetos más pequeños y observe.",
        type: "MF",
        image: "10b"
    },
    {
        month: 10,
        title: "NIEGA CON LA CABEZA",
        message: "Observe durante la evaluación o pregunte si la niña o niño niega con la cabeza cuando no quiere algo.",
        type: "AL",
        image: "10c"
    },
    {
        month: 10,
        title: "ENTREGA UN OBJETO AL EVALUADOR",
        message: "Dele un objeto, y después de un momento pídale que le devuelva.",
        type: "PS",
        image: "10d"
    },
    {
        month: 11,
        title: "SE PARA AGARRÁNDOSE DE ALGO (HASTA CONTAR 10)",
        message: "Hágale sentar cerca de algo firme, y muéstrele un objeto que le guste.",
        type: "MG",
        image: "11a"
    },
    {
        month: 11,
        title: "AGARRA 3 OBJETOS SIN SOLTARLOS (HASTA CONTAR 3)",
        message: "Dele un objeto pequeño en cada mano y ofrézcale un tercer objeto.",
        type: "MF",
        image: "11b"
    },
    {
        month: 11,
        title: "LLAMA A LA PERSONA QUE LE CUIDA",
        message: "Observe durante la evaluación si la niña o el niño llamó por su nombre o de otra forma a la persona que le cuida.",
        type: "AL",
        image: "11c"
    },
    {
        month: 11,
        title: "PIDE UN OBJETO QUE QUIERE O NECESITA",
        message: "Ponga varios objetos a la vista de la niña o el niño. Debe pedir alguno aunque no hable bien.",
        type: "PS",
        image: "11d"
    },
    {
        month: 12,
        title: "SE MANTIENE DE PIE SIN APOYARSE (HASTA CONTAR 10)",
        message: "Ayúdele a ponerse de pie, entrégele un objeto que le guste.",
        type: "MG",
        image: "12a"
    },
    {
        month: 12,
        title: "BUSCA OBJETOS ESCONDIDOS (2 VECES)",
        message: "Muéstrele un objeto y escóndalo debajo de una manta o aguayo.",
        type: "MF",
        image: "12b"
    },
    {
        month: 12,
        title: "ENTIENDE UNA ORDEN SENCILLA",
        message: "Pídale que haga algo. Ejemplo: Dame el vaso, guarda la muñeca, etc.",
        type: "AL",
        image: "12c"
    },
    {
        month: 12,
        title: "TOMA SOLA O SOLO AGARRANDO LA TAZA",
        message: "Pregunte a la madre o al padre si agarra el vaso o taza para tomar sola o solo.",
        type: "PS",
        image: "12d"
    },
    {
        month: 13,
        title: "CAMINA 5 PASOS (SOLA O SOLO)",
        message: "Póngale de pie con cuidado, suéltele y llame su atención con un objeto.",
        type: "MG",
        image: "13a"
    },
    {
        month: 13,
        title: "HACE TORRES DE 3 CUBOS",
        message: "Enséñele cómo hacer torres con los cubos.",
        type: "MF",
        image: "13b"
    },
    {
        month: 13,
        title: "RECONOCE 3 OBJETOS",
        message: "Ponga varios objetos delante de la niña o el niño. Pídale de uno en uno hasta que entregue o señale tres.",
        type: "AL",
        image: "13c"
    },
    {
        month: 13,
        title: "SEÑALA UNA PRENDA DE VESTIR",
        message: "Pregunte: ¿Cuál es tu chompa?.¿cuál es tu pantalón?, etc.",
        type: "PS",
        image: "13d"
    },
    {
        month: 14,
        title: "CAMINA SIN AYUDA",
        message: "Póngale de pie y llame su atención con un objeto. Debe caminar sin arrastrar los pies.",
        type: "MG",
        image: "14a"
    },
    {
        month: 14,
        title: "PASA HOJA POR HOJA UN CUENTO",
        message: "Enséñale cómo hojear un cuento. La niña o el niño debe hojear con la punta de los dedos.",
        type: "MF",
        image: "14b"
    },
    {
        month: 14,
        title: "COMBINA 2 PALABRAS",
        message: "Escuche atentamente o pregunte a la familia si la niña o el niño habla dos palabras juntas: más leche, etc.",
        type: "AL",
        image: "14c"
    },
    {
        month: 14,
        title: "SEÑALA 2 PARTES DE SU CUERPO",
        message: "Pida a la niña o al niño que le muestre sus ojos, manos, nariz, etc.",
        type: "PS",
        image: "14d"
    },
    {
        month: 15,
        title: "CORRE",
        message: "Invite a la niña o al niño a correr. Debe correr sin arrastrar los pies.",
        type: "MG",
        image: "15a"
    },
    {
        month: 15,
        title: "ESPERA QUE SALGA LA PELOTITA (2 VECES)",
        message: "Meta la pelotita por un lado del tubo. La niña o el niño debe esperar al otro lado con sus manos.",
        type: "MF",
        image: "15b"
    },
    {
        month: 15,
        title: "RECONOCE 6 OBJETOS",
        message: "Póngale varios objetos delante de la niña o el niño. Pídale de uno en uno hasta que entregue o señale seis.",
        type: "AL",
        image: "15c"
    },
    {
        month: 15,
        title: "COME SOLA O SOLO",
        message: "Observe a la niña o al niño o pregunte a la familia si come sola o solo usando la cuchara.",
        type: "PS",
        image: "15d"
    },
    {
        month: 16,
        title: "PATEA LA PELOTA",
        message: "Coloque la pelota delante de la niña o el niño, y pídale que patee donde usted le indique.",
        type: "MG",
        image: "16a"
    },
    {
        month: 16,
        title: "TAPA BIEN UN RECIPIENTE (2 VECES)",
        message: "Enséñele cómo tapar un recipiente. La niña o el niño debe taparlo bien.",
        type: "MF",
        image: "16b"
    },
    {
        month: 16,
        title: "NOMBRA 5 OBJETOS",
        message: "Pregunte a la niña o niño los nombres de objetos. Debe decir 5 nombres aunque no pronuncie bien.",
        type: "AL",
        image: "16c"
    },
    {
        month: 16,
        title: "SEÑALA 5 PARTES DE SU CUERPO",
        message: "Pídale a la niña o al niño que le muestre: su nariz, cabeza, ojos, manos, dientes, etc.",
        type: "PS",
        image: "16d"
    },
    {
        month: 17,
        title: "LANZA LA PELOTA CON LAS MANOS (2 VECES)",
        message: "Dele la pelota y ubíquese a 2 ó 3 pasos delante de la niña o el niño, y dígale que le pase la pelota.",
        type: "MG",
        image: "17a"
    },
    {
        month: 17,
        title: "HACE GARABATOS REDONDOS",
        message: "Dele lápiz y papel, enséñele cómo hacer garabatos. No importa cómo agarre el lápiz.",
        type: "MF",
        image: "17b"
    },
    {
        month: 17,
        title: "USA FRASES DE 3 PALABRAS",
        message: "Escuche atentamente o pregunte a la familia si la niña o el niño habla con frases de 3 palabras como:'mamá quiero comida'.",
        type: "AL",
        image: "17c"
    },
    {
        month: 17,
        title: "TRATA DE CONTAR LO QUE HA VISTO O LO QUE HA HECHO",
        message: "Pregunte a la niña o al niño: ¿Qué has hecho?, ¿qué has visto?, etc.",
        type: "PS",
        image: "17d"
    },
    {
        month: 18,
        title: "SALTA CON LOS PIES JUNTOS (2 VECES)",
        message: "Enséñele a saltar con los dos pies al mismo tiempo. La niña o el niño debe saltar igual.",
        type: "MG",
        image: "18a"
    },
    {
        month: 18,
        title: "HACE TORRES DE 5 ó MÁS CUBOS",
        message: "Enséñele cómo hacer torres con los cubos.",
        type: "MF",
        image: "18b"
    },
    {
        month: 18,
        title: "DICE MÁS DE 20 PALABRAS CLARAMENTE",
        message: "Escuche o pregunte a la familia si la niña o el niño habla. Debe decir por lo menos 20 palabras.",
        type: "AL",
        image: "18c"
    },
    {
        month: 18,
        title: "CONTROLA EN EL DÍA SU ORINA",
        message: "Alguna vez la niña o el niño puede orinarse en su ropa en el día.",
        type: "PS",
        image: "18d"
    },
    {
        month: 19,
        title: "SE PARA DE PUNTAS (HASTA CONTAR 5)",
        message: "Enséñele el ejercicio. La niña o el niño, debe repetir sin perder el equilibrio ni apoyarse.",
        type: "MG",
        image: "19a"
    },
    {
        month: 19,
        title: "ENSARTA 6 ó MÁS CUENTAS",
        message: "Enséñele cómo ensartar unas cuentas en una pita.",
        type: "MF",
        image: "19b"
    },
    {
        month: 19,
        title: "DICE SU NOMBRE COMPLETO",
        message: "Pregúntele: ¿Cómo te llamas?. La niña o niño debe responder su nombre y por lo menos uno de sus apellidos.",
        type: "AL",
        image: "19c"
    },
    {
        month: 19,
        title: "DIFERENCIA ENTRE NIÑA Y NIÑO",
        message: "Pregunte: ¿Eres niña?... ¿Eres niño? Ambas respuestas deben ser correctas.",
        type: "PS",
        image: "19d"
    },
    {
        month: 20,
        title: "SE PARA SIN USAR LAS MANOS",
        message: "Anime a que la niña o niño se ponga de rodillas, levante las manos y que se pare. Puede enseñarle primero.",
        type: "MG",
        image: "20a"
    },
    {
        month: 20,
        title: "COPIA RAYAS ECHADAS Y PARADAS (3 VECES )",
        message: "Muéstrele la lámina y dígale que copie. No es necesario que sean perfectas.",
        type: "MF",
        image: "20b"
    },
    {
        month: 20,
        title: "DIFERENCIA ALTO-BAJO, GRANDE-PEQUEÑO",
        message: "Muéstrele dos torres: una alta, otra baja y un objeto grande y otro pequeño. La niña o el niño debe diferenciar.",
        type: "AL",
        image: "20c"
    },
    {
        month: 20,
        title: "DICE EL NOMBRE DE SU PAPÁ Y DE SU MAMÁ",
        message: "Pregunte a la niña o al niño: ¿Cómo se llama tu mamá?... ¿y tu papá? Debe responder correctamente.",
        type: "PS",
        image: "20d"
    },
    {
        month: 21,
        title: "CAMINA HACIA ATRÁS (5 PASOS)",
        message: "Enséñele el ejercicio. La niña o el niño debe caminar sin pararse, tropezar ni perder el equilibrio.",
        type: "MG",
        image: "21a"
    },
    {
        month: 21,
        title: "SEPARA OBJETOS GRANDES DE LOS PEQUEÑOS",
        message: "Dele las figuras geométricas y dígale, aquí pon los pequeños y aquí los grandes.",
        type: "MF",
        image: "21b"
    },
    {
        month: 21,
        title: "USA ORACIONES COMPLETAS",
        message: "Escuche durante la evaluación. La niña o el niño debe hablar ideas completas.",
        type: "AL",
        image: "21c"
    },
    {
        month: 21,
        title: "SE LAVA LAS MANOS Y LA CARA SIN AYUDA",
        message: "Observe a la niña o al niño o pregunte a la familia si se lava la cara y manos sola o solo.",
        type: "PS",
        image: "21d"
    },
    {
        month: 22,
        title: "CAMINA DE PUNTAS (2 METROS)",
        message: "Enséñele el ejercicio. La niña o el niño debe caminar de puntitas sin pararse, ni perder el equilibrio.",
        type: "MG",
        image: "22a"
    },
    {
        month: 22,
        title: "DIBUJA EL CUERPO HUMANO CON 3 PARTES",
        message: "Pidale a la niña o al niño que dibuje a una persona. El dibujo debe tener ojos, boca, brazos, etc.",
        type: "MF",
        image: "22b"
    },
    {
        month: 22,
        title: "CONOCE PARA QUÉ SIRVEN 5 OBJETOS",
        message: "Agarrando los objetos uno por uno, pregúntele: ¿para qué sirve esto?, ¿y ésto?",
        type: "AL",
        image: "22c"
    },
    {
        month: 22,
        title: "PUEDE DESVESTIRSE SIN AYUDA",
        message: "Pregunte a la madre o al padre si la niña o el niño se desviste sola o solo.",
        type: "PS",
        image: "22d"
    },
    {
        month: 23,
        title: "SE PARA EN UN SOLO PIE (HASTA CONTAR 5, 2 VECES)",
        message: "La niña o el niño debe pararse en un pie, sin perder el equilibrio.",
        type: "MG",
        image: "23a"
    },
    {
        month: 23,
        title: "CORTA PAPEL CON LA TIJERA",
        message: "Pídale que corte un papel. Debe cortar hasta el otro lado sin romperlo.",
        type: "MF",
        image: "23b"
    },
    {
        month: 23,
        title: "REPITE TRES NÚMEROS",
        message: "Pídale que ponga atención y que repita correctamente; por ejemplo: 1-3-5, 9-4-7, etc.",
        type: "AL",
        image: "23c"
    },
    {
        month: 23,
        title: "JUEGA CON OTRAS NIÑAS Y NIÑOS",
        message: "Pregunte a la niña o al niño si juega con otras niñas y niños. La familia puede informar sobre esta actividad.",
        type: "PS",
        image: "23d"
    },
    {
        month: 24,
        title: "TIRA Y AGARRA LA PELOTA (2 VECES)",
        message: "Póngale a una distancia de 2 metros y enséñele el ejercicio.",
        type: "MG",
        image: "24a"
    },
    {
        month: 24,
        title: "COPIA CUADRADOS Y CÍRCULOS",
        message: "El cuadrado debe tener las 4 esquinas, y el círculo líneas unidas.",
        type: "MF",
        image: "24b"
    },
    {
        month: 24,
        title: "DESCRIBE BIEN EL DIBUJO",
        message: "Muéstrele un dibujo de un paisaje que tenga varias cosas. La niña o el niño debe contar como cuento o historia.",
        type: "AL",
        image: "24c"
    },
    {
        month: 24,
        title: "TIENE AMIGAS O AMIGOS ESPECIALES",
        message: "Pregunte si tiene amigas o amigos. Debe decir por lo menos un nombre. (No de sus hermanos o hermanas).",
        type: "PS",
        image: "24d"
    },
    {
        month: 25,
        title: "CAMINA SOBRE UNA LÍNEA RECTA (2 METROS)",
        message: "Enséñele el ejercicio, un pie frente al otro. La niña o el niño camina un pie frente al otro sin perder el equilibrio.",
        type: "MG",
        image: "25a"
    },
    {
        month: 25,
        title: "DIBUJA EL CUERPO HUMANO CON 5 PARTES",
        message: "Dígale que dibuje a una persona. El dibujo debe tener 5 partes claras: cabeza, ojos, boca, nariz, y brazos.",
        type: "MF",
        image: "25b"
    },
    {
        month: 25,
        title: "CUENTA HASTA 10",
        message: "Pídale que cuente los dedos de sus manos u otros objetos. La niña o el niño debe contar sin equivocarse.",
        type: "AL",
        image: "25c"
    },
    {
        month: 25,
        title: "PUEDE VESTIRSE Y DESVESTIRSE SIN AYUDA",
        message: "Pregunte a la madre o al padre sobre esta actividad (no se toma en cuenta botones ni cordones).",
        type: "PS",
        image: "25d"
    },
    {
        month: 26,
        title: "SALTA EN UN PIE (3 VECES)",
        message: "Enséñele el ejercicio. La niña debe saltar, sin perder el equilibrio.",
        type: "MG",
        image: "26a"
    },
    {
        month: 26,
        title: "AGRUPA POR COLOR Y FORMA",
        message: "Pídale que forme grupos con las figuras geométricas: por color y forma al mismo tiempo.",
        type: "MF",
        image: "26b"
    },
    {
        month: 26,
        title: "DIFERENCIA ADELANTE – ATRÁS, ARRIBA-ABAJO",
        message: "Dígale: 'Muéstrame lo que está delante de la caja…, lo que está detrás…, arriba y abajo'.",
        type: "AL",
        image: "26c"
    },
    {
        month: 26,
        title: "SABE CUÁNTOS AÑOS TIENE",
        message: "Pregunte a la niña o niño: ¿Cuántos años tienes? la respuesta puede ser hablando o con los dedos.",
        type: "PS",
        image: "26d"
    },
    {
        month: 27,
        title: "HACE REBOTAR Y AGARRA LA PELOTA (2 VECES)",
        message: "Enséñele cómo hacer rebotar y agarrar la pelota.",
        type: "MG",
        image: "27a"
    },
    {
        month: 27,
        title: "COPIA UNA ESCALERA (CON 5 TRAVESAÑOS)",
        message: "Debe hacerlo con líneas rectas y claras en la misma posición.",
        type: "MF",
        image: "27b"
    },
    {
        month: 27,
        title: "NOMBRA 5 COLORES",
        message: "Utilice los objetos que tiene y pregúntele: ¿Qué color es éste?... y ¿éste?, etc.",
        type: "AL",
        image: "27c"
    },
    {
        month: 27,
        title: "ORGANIZA JUEGOS",
        message: "Pregunte a la madre si su niña o niño organiza a sus amiguitos para jugar.",
        type: "PS",
        image: "27d"
    },
    {
        month: 28,
        title: "SALTA CON LOS PIES JUNTOS (2 VECES)",
        message: "Dos personas agarran la soga a 25 cm del piso. Enséñele a la niña o al niño a saltar con los pies juntos.",
        type: "MG",
        image: "28a"
    },
    {
        month: 28,
        title: "AGRUPA POR COLOR, FORMA Y TAMAÑO",
        message: "Pídale que forme grupos con las figuras geométricas, por forma, tamaño y color, al mismo tiempo.",
        type: "MF",
        image: "28b"
    },
    {
        month: 28,
        title: "EXPRESA OPINIONES",
        message: "Pregunte a la niña: ¿Qué te gusta hacer o jugar? debe responder dando su propia opinión.",
        type: "AL",
        image: "28c"
    },
    {
        month: 28,
        title: "HACE MANDADOS",
        message: "Pregunte a la niña o al niño en qué cosas ayuda en la casa. La niña o el niño dice en qué cosas ayuda.",
        type: "PS",
        image: "28d"
    },
    {
        month: 29,
        title: "CORRE COMBINANDO LOS PIES (4 METROS)",
        message: "Enséñele a correr combinando los pies, como caballito, sin equivocarse ni perder el ritmo.",
        type: "MG",
        image: "29a"
    },
    {
        month: 29,
        title: "CONSTRUYE GRADAS CON 10 CUBOS",
        message: "Demuéstrele el ejercicio. La niña o el niño debe hacer lo mismo.",
        type: "MF",
        image: "29b"
    },
    {
        month: 29,
        title: "CONOCE SU IZQUIERDA Y DERECHA (5 VECES)",
        message: "Pregúntele: ¿Cuál es tu mano derecha?, ¿tu ojo izquierdo?, ¿tu pie derecho?, etc.",
        type: "AL",
        image: "29c"
    },
    {
        month: 29,
        title: "CONOCE EL NOMBRE DE SU COMUNIDAD",
        message: "Pregunte a la niña o al niño: ¿Cómo se llama esta comunidad? Debe responder correctamente.",
        type: "PS",
        image: "29d"
    },
    {
        month: 30,
        title: "SALTA DESDE 50 CM DE ALTURA",
        message: "La niña o el niño debe saltar y caer de pie sin apoyarse en las manos.",
        type: "MG",
        image: "30a"
    },
    {
        month: 30,
        title: "DIBUJA UNA CASA CON 3 PARTES",
        message: "Pídale que dibuje una casa. El dibujo debe tener paredes, techo, puerta, ventanas, etc.",
        type: "MF",
        image: "30b"
    },
    {
        month: 30,
        title: "CONOCE 6 DÍAS DE LA SEMANA",
        message: "Pregunte los nombres de los días de la semana. La niña o el niño debe nombrarlos, sin importar el orden.",
        type: "AL",
        image: "30c"
    },
    {
        month: 30,
        title: "COMENTA LA VIDA FAMILIAR",
        message: "Pregunte sobre las actividades que hace toda su familia. La niña o el niño debe contar lo que piensa.",
        type: "PS",
        image: "30d"
    }
]

export const QUESTION_LIST = questionList;