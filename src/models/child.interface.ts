export interface Child {
    idChild:number;
    names: string;
    lastName: string;
    secondName: string;
    sexo: string;
    peso: string;
    talla: string;
    birthDate: string;
} 