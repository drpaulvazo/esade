export interface Question{
    month:number;
    title:string;
    message:string;
    type:string;
    image:string;
} 