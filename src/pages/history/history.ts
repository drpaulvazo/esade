import {Child} from '../../models/child.interface';
import {History} from '../../models/history.interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HistoryProvider } from '../../providers/history/history';
import { ResultPage } from '../result/result';
@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  historyList: History[];
  currentChild:Child;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private historyService:HistoryProvider) {
    this.currentChild = this.navParams.get('child');
    this.historyService.mockGetHistory(this.currentChild.idChild)
      .subscribe(data => 
        this.historyList = data,
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }
  goToResult(history:History){
    this.navCtrl.push(ResultPage,{'history':history});
  }
}
