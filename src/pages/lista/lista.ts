import { QuizPage } from '../quiz/quiz';
import { RegistroPage } from '../registro/registro';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChildrenProvider } from '../../providers/children/children';
import { Child } from '../../models/child.interface';
import { HistoryPage } from '../history/history';

@IonicPage()
@Component({
  selector: 'page-lista',
  templateUrl: 'lista.html',
})

export class ListaPage {
  childrenList: Child[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private childrenService: ChildrenProvider) {
    this.childrenService.mockGetUserInformation()
      .subscribe(data =>
        this.childrenList = data,
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaPage');
  }


  setItems() {
    this.childrenService.mockGetUserInformation()
      .subscribe(data =>
        this.childrenList = data,
    );
  }
  ngOnInit() {
    this.setItems();
  }
  filterItems(ev: any) {
    this.setItems();
    let val = ev.target.value;
    if (val && val.trim() !== '') {
      this.childrenList = this.childrenList.filter(function (child) {
        return child.names.toLowerCase().includes(val.toLowerCase());
      });
    }
  }

  goToRegister(): void {
    this.navCtrl.push(RegistroPage);
  }
  goTest(child: Child): void {
    this.navCtrl.push(QuizPage, { 'child': child })
  }
  goHistory(child: Child): void {
    this.navCtrl.push(HistoryPage, { 'child': child });
  }
}
