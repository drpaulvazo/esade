import { ListaPage } from '../lista/lista';
import { Component } from '@angular/core';
import { ToastController, LoadingController, AlertController, Loading, IonicPage, NavController} from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../models/user.interface';
import { LoginProvider } from '../../providers/login.service';

import * as _ from "lodash";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  myForm: FormGroup;
  user: User;
  public loading: Loading;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private loginService: LoginProvider,
    private toastCtrl: ToastController
  ) {
    this.myForm = this.formBuilder.group({
      email: ['paulwilkerlf@gmail.com', Validators.required],
      password: ['123456', Validators.required]
    });
    //this.user = afAuth.authState;
  }

  getUserInformation(): void {
    console.log('funcion');
    this.loginService.mockGetUserInformation(this.myForm.value.email)
      .subscribe(data =>
        this.user = data,
    );
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'User is Incorrect',
      duration: 3000,
      position: 'top',
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  loginUser() {

    console.log("Email:" + this.myForm.value.email);
    console.log("Password:" + this.myForm.value.password);
    this.getUserInformation();
    console.log(this.user);
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
    });

    this.loading.present();
    console.log(this.user);
    if (!_.isNil(this.user) && this.user.password == this.myForm.value.password) {
      console.log('Is Correct');
      this.loading.dismiss();
      this.loginService.currentUser = this.user;
      this.navCtrl.setRoot(ListaPage);
    }
    else {
      this.loading.dismiss();
      this.presentToast();
    }

  }



}
