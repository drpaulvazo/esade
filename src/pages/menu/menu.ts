import {LoginProvider} from '../../providers/login.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../models/user.interface';
import { RegistroPage } from '../registro/registro';
import { ListaPage } from '../lista/lista';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  private currentUser : User;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private loginService:LoginProvider) {
              this.currentUser = this.loginService.currentUser;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }
  goToRegister():void{
    this.navCtrl.push(RegistroPage);
  }
  goToList():void{
    this.navCtrl.push(ListaPage);
  }
}
