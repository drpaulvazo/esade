import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Question } from '../../models/question.interface';
import { QuestionsProvider } from '../../providers/questions/questions.service';
import { Child } from '../../models/child.interface';
import { EDAD } from '../../app/edad';
import { ResultPage } from '../result/result';
import { HistoryProvider } from '../../providers/history/history';


@IonicPage()
@Component({
  selector: 'page-question',
  templateUrl: 'question.html',
})
export class QuestionPage {
  questionList: Question[];
  currentQuestion: Question;
  month: number;
  capacity: string;
  currentChild: Child;
  type: string;
  noAnswer: number;
  yesAnswer: number;
  totalQuestions: number;
  isUpQuestion: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private questionService: QuestionsProvider,
    private alertCtrl: AlertController) {
    
    this.capacity = this.navParams.get('capacity');
    this.currentChild = this.navParams.get('child');
    var dateChild = new Date(this.currentChild.birthDate);
    var currentDate = new Date();
    this.month = this.getMonths(dateChild, currentDate);
    this.month = EDAD.edad[this.month];
    this.questionService.mockGetUserInformation()
      .subscribe(data =>
        this.questionList = data,
    );
    this.getQuestionByMonth(this.month, this.capacity);
    this.totalQuestions = 3;
    this.isUpQuestion = true;
    this.noAnswer = 0;
    this.yesAnswer = 0;
  }
  private getQuestionByMonth(month: number, capacity: string) {
    this.questionService.mockGetQuestion(month, capacity)
      .subscribe(data =>
        this.currentQuestion = data,
    );
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionPage');
  }
   public min=this.getMonths;
  nextQuestion(isYes: boolean) {
  
    if (this.isUpQuestion) {
      if (isYes) {
        this.yesAnswer++;
      }
      else {
        this.yesAnswer= 0;
      }
      if (this.yesAnswer === this.totalQuestions) {
        this.isUpQuestion = false;
        this.alertDescendente();
        console.log('finish up');
        this.getQuestionByMonth(this.month + 1, this.capacity);
      }
      else {
        this.getQuestionByMonth(this.currentQuestion.month - 1, this.capacity);
      }
    }
    else {
      if (isYes) {
        this.noAnswer = 0;
      }
      else {
        this.noAnswer++;
      }
      if (this.noAnswer === this.totalQuestions) {
        this.alertFinalizarTest();
        console.log('finish');
      }
      else {
        this.getQuestionByMonth(this.currentQuestion.month + 1, this.capacity);
      }
    }
  }
  private alertDescendente() {
    let alert = this.alertCtrl.create({
      title: 'Evaluacion Ascendente Terminada!',
      subTitle: 'Comenzando evaluacion descendente!',
      buttons: ['OK']
    });
    alert.present();
  }

  private alertFinalizarTest() {
    let obj = {nombre:'paul'};
    let alert = this.alertCtrl.create({
      title: 'Evaluacion Finalizada!',
      subTitle: 'Usted ha finalizado la evaluacion!',
      buttons: [{
        text: 'OK',
        handler: () => {
          this.navCtrl.pop();
        }
      }]
    });
    alert.present();
  }

  private getMonths(birthDate: Date, currentDate: Date): number {
    var months:number;
    months = (currentDate.getFullYear() - birthDate.getFullYear()) * 12;
    months -= birthDate.getMonth() + 1;
    months += currentDate.getMonth();
    return months < 0 ? -1 : months;
  }
}
