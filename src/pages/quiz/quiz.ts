import {HistoryProvider} from '../../providers/history/history';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionPage } from '../question/question';
import { Child } from '../../models/child.interface';
import { HistoryPage } from '../history/history';
import { ResultPage } from '../result/result';
import {History} from '../../models/history.interface'

@IonicPage()
@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html',
})
export class QuizPage {
  currentChild:Child;
  history: History;
  constructor(public navCtrl: NavController, public navParams: NavParams,private historyService:HistoryProvider) {
    this.currentChild = this.navParams.get('child');
      var currentDate = new Date();
    //this.getHistory(this.currentChild.idChild,currentDate.toString());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuizPage');
  }

  private getHistory(idChild:number,date:string){
    this.historyService.mockGetHistoryByDate(idChild,date)
      .subscribe(data => 
        this.history = data,
    );
    console.log(this.history);
  }
  goQuestions(capacity:string){
    this.navCtrl.push(QuestionPage,{'capacity':capacity,
    'child':this.currentChild});
  }

  goResult(){
    console.log('go result Page');
    this.navCtrl.push(ResultPage);
  }
}
