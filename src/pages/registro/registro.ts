import {ChildrenProvider} from '../../providers/children/children';
import { Component, ViewChild } from '@angular/core';
import {AlertController, IonicPage,  NavController,  NavParams} from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Child } from '../../models/child.interface';

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {
  @ViewChild('signupSlider') signupSlider: any;
  
  slideOneForm: FormGroup;
  submitAttempt: boolean = false;
currentChild: Child[]=[
  {
      idChild:1,
      names:'Paul Wilker',
      lastName:'Landaeta',
      secondName:'Flores',
      sexo:'masculino',
      birthDate:'25/03/2017',
      peso:'60',
      talla:'170',
  }];
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public formBuilder: FormBuilder,
     private childrenService:ChildrenProvider,
     public alertCtrl: AlertController) {
    this.slideOneForm = formBuilder.group({
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      names: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],   
      age: [''],
      peso:[''],
      talla:['20'],
      sexo:['Masculino'],
      evalDate: ['2016-01-01', Validators.required]
  });
  }
  next(){
      this.signupSlider.slideNext();
  }

  prev(){
      this.signupSlider.slidePrev();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }
  addChild(){
    console.log(this.slideOneForm.value);
    console.log(this.currentChild);

    this.currentChild[0].lastName = this.slideOneForm.value.lastName;
    this.currentChild[0].names = this.slideOneForm.value.names;
    this.currentChild[0].secondName = this.slideOneForm.value.secondName;
    this.currentChild[0].peso = this.slideOneForm.value.peso;
    this.currentChild[0].talla = this.slideOneForm.value.talla;
    this.currentChild[0].sexo = this.slideOneForm.value.sexo;
    this.currentChild[0].birthDate = this.slideOneForm.value.evalDate;
    this.childrenService.addChild(this.currentChild[0]);
  
    let alert = this.alertCtrl.create({
      title: 'Nuevo Niño!',
      subTitle: 'Usted ha agregado a un nuevo Niño!',
      buttons: ['OK']
    });
    alert.present();

  }
}
