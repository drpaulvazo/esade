import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ListaPage } from '../lista/lista';
import { History } from '../../models/history.interface';
@IonicPage()
@Component({
  selector: 'page-result',
  templateUrl: 'result.html',
})
export class ResultPage {
  currentHistory:History;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.currentHistory = this.navParams.get('history');
    console.log(this.currentHistory);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultPage');
  }
  goToList(){
    this.navCtrl.setRoot(ListaPage);
  }

}
