import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Child } from '../../models/child.interface';
import { CHILD_LIST } from '../../mocks/child.mocks';
@Injectable()
export class ChildrenProvider {
  
  constructor() {
    console.log('Hello ChildrenProvider Provider');
  }
  mockGetUserInformation():Observable<Child[]>{
    return Observable.of(CHILD_LIST)
  }
  addChild(child:Child):void{
    CHILD_LIST.push(child);
  }
}
