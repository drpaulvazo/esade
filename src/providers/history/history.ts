import {History} from '../../models/history.interface';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HISTORY_LIST } from '../../mocks/history.mocks';


@Injectable()
export class HistoryProvider {

  constructor() {
    console.log('Hello HistoryProvider Provider');
  }
  mockGetHistoryInformation():Observable<History[]>{
    return Observable.of(HISTORY_LIST)
  }

  mockGetHistory(idChild:number):Observable<History[]>{
    return Observable.of(HISTORY_LIST.filter(history => history.idChild === idChild))
  }
  mockGetHistoryByDate(idChild:number,date:string):Observable<History>{
    return Observable.of(HISTORY_LIST.filter(history => history.idChild === idChild && history.date === date)[0])
  }
}
