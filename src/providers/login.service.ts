import { Injectable } from '@angular/core';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/Observable/of';


import { User } from '../models/user.interface';
import { USER_LIST } from '../mocks/user.mocks';
 /*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {
  public currentUser:User;
  constructor() {
    console.log('Hello LoginProvider Provider');
  }
  mockGetUserInformation(username:string):Observable<User>{
    return Observable.of(USER_LIST.filter(user => user.email === username)[0])
  }

}
