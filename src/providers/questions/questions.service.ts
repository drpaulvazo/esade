import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Question } from '../../models/question.interface';
import { QUESTION_LIST } from '../../mocks/question.mock';

@Injectable()
export class QuestionsProvider {

    constructor() {
        console.log('Hello QuestionsProvider Provider');
    }

    mockGetUserInformation(): Observable<Question[]> {
        return Observable.of(QUESTION_LIST)
    }

    addChild(question: Question): void {
        QUESTION_LIST.push(question);
    }
    mockGetQuestion(questionNumber:number,capacity:string):Observable<Question>{
        return Observable.of(QUESTION_LIST.filter(question => question.month === questionNumber && question.type=== capacity)[0])
    }
}
